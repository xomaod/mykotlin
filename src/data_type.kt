fun main(args: Array<String>){

    // String
    val firstname: String = "Jaturong"
    val lastname: String = "Vachirasasakul"

    println("You are ${firstname} ${lastname}")

    // Int
    val age: Int = 128
    val height: Int = 167

    println(age + height)

    // Print out Args
    if(args.size == 0)
        println("Please enter some arguments.")
    else
        println("Your args is " + args[0])

    // Array 1D
    var i: Int = 0
    val matrixs: Array<String> = arrayOf("Zero","One","Two","Three","Four")

    for(matrix in matrixs){
        println(matrix)
    }

    // Lambda expression function
    val sumLambda: (Int, Int) -> Int = {x,y -> x+y}
    val actualSum = sumLambda(10,12)

    println("Actual Sum is ${actualSum}")


}